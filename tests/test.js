const Application = require('spectron').Application;
const path = require('path');
const chai = require('chai');
var expect = require('chai').expect;

require('mocha-sinon');
const chaiAsPromised = require('chai-as-promised');

var electronPath = path.join(__dirname, '..', 'release', 'git-harpon.app/Contents/MacOS/git-harpon');

if (process.platform === 'linux') {
    electronPath = path.join(__dirname, '..', 'release', 'git-harpon-5.1.0.AppImage');
} else if (process.platform === 'darwin') {
    electronPath = path.join(__dirname, '..', 'release', 'git-harpon.app/Contents/MacOS/git-harpon');
} else if (process.platform === 'win32') {
    electronPath = path.join(__dirname, '..', 'release', 'git-harpon-5.1.0.exe');
}

var appPath = path.join(__dirname, '..');

var app = new Application({
    path: electronPath,
    args: [appPath]
});

global.before(function () {
    chai.should();
    chai.use(chaiAsPromised);
});

describe('Test Example', function () {
    beforeEach(function () {
        var log = console.log;
        this.sinon.stub(console, 'log').callsFake( function() {
          return log.apply(log, arguments);
        });
        return app.start();
    });
  
    afterEach(function () {
        return app.stop();
    });
  
    it('opens a window', function () {
      return app.client.waitUntilWindowLoaded()
        .getWindowCount().should.eventually.equal(1);
    });
  
    it('tests the title', function () {
      return app.client.waitUntilWindowLoaded()
        .getTitle().should.eventually.equal('AngularElectron');
    });

    it('test console log', function() {
        app.client.getText('#content').then(function(text) {
            console.log(text);
        });
        return app.client.waitUntilWindowLoaded() 
       .getRenderProcessLogs().then(function (logs) { 
            expect( console.log.calledWith('coucou mon ptit pote') ).to.be.true;        
       });
      });

    it('test button change angular', function () {
        var btn = app.client.element('#btntest');
        var btntext = app.client.getText('#btntest').then(function(text){
            expect(text).to.be.equal('test1');
        });
        btn.click();
        btntext = app.client.getText('#btntest').then(function(text){
            expect(text).to.be.equal('test2');
        });
    });
  });