import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-test',
  templateUrl: './button-test.component.html',
  styleUrls: ['./button-test.component.scss']
})
export class ButtonTestComponent implements OnInit {

  constructor() { }

  public text: String = 'test1';

  public testbtn(): void {
    if(this.text === 'test1') { 
      this.text = 'test2'
    } else {
      this.text = 'test1'
    }
  }
  ngOnInit() {
    this.testbtn();
  }

}
